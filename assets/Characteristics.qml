/***************************************************************************
**
** Copyright (C) 2013 BlackBerry Limited. All rights reserved.
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the QtBluetooth module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.12
import Qt.labs.settings 1.0

Page {
    Component.onCompleted: {
        titleLabel.text = "Characteristic List"
    }
    Connections {
        target: device
        onCharacteristicScanStateChanged: {
            if(!device.characteristicScanState) {
                scanIndicator.running = false
            }
        }
        onDisconnected: {
            console.log("Disconnected")
            scanIndicator.running = false
        }
    }
    ListView {
        id: theServiceListView
        clip: true
        anchors.fill: parent
        model: device.characteristicList
        delegate: ItemDelegate {
            width: parent.width
            height: 120
            onClicked: {
                inputDialog.setCharacteristic(modelData, index,"Write to "
                                              + modelData.characteristicName
                                              + " characteristic:")
                inputDialog.open()
            }
            ColumnLayout {
                anchors.fill: parent
                ColumnLayout {
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Layout.margins: 5
                    RowLayout {
                        Label {
                            id: characteristicName
                            text: modelData.characteristicName
                            font.pointSize: 14
                        }
                        Label {
                            id: characteristicPermission
                            font.pointSize: characteristicName.font.pointSize*0.6
                            text: modelData.characteristicPermission
                        }
                    }

                    Label {
                        id: characteristicUuid
                        font.pointSize: characteristicName.font.pointSize*0.7
                        text: modelData.characteristicUuid
                         opacity: 0.54
                    }
                    RowLayout {

                        Label {
                            id: characteristicValue
                            Layout.fillWidth: true
                            font.pointSize: characteristicName.font.pointSize*0.7
                            text: {
                                return "Value: " + (modelData.characteristicValue.trim() ? "": "0x") + modelData.characteristicValue.trim()
                            }
                        }
                        Label {
                            id: characteristicHandle
                            Layout.fillWidth: true
                            font.pointSize: characteristicName.font.pointSize*0.7
                            text: ("Handlers: " + modelData.characteristicHandle)
                        }
                    }
                }
                Rectangle {
                    Layout.fillWidth: true
                    Layout.preferredHeight: 1
                    opacity: 0.1
                }
            }
        }
    }
    Dialog {
        id: inputDialog
        property var data
        property var index
        function setCharacteristic(data, index, text) {
            inputDialog.data = data
            inputDialog.index = index
            dialogText.text = text
            inputValue.text = ""
        }
        x: (parent.width - width) / 2
        y: (parent.height - height) / 2
        width: 300
        parent: Overlay.overlay
        focus: true
        modal: true
        title: "Write value"
        standardButtons: Dialog.Ok | Dialog.Cancel
        ColumnLayout {
            spacing: 20
            anchors.fill: parent
            Label {
                id: dialogText
                elide: Label.ElideRight
                Layout.fillWidth: true
            }
            TextField {
                id: inputValue
                focus: true
                placeholderText: "Hex value"
                Layout.fillWidth: true
            }
        }
        onAccepted: {
            console.log("Index is " + inputDialog.index + " and value is " + inputValue.text)
        }
    }
}
