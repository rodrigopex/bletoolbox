/***************************************************************************
**
** Copyright (C) 2013 BlackBerry Limited. All rights reserved.
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the QtBluetooth module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.12
import Qt.labs.settings 1.0

import "Icon.js" as MdiFont

ApplicationWindow {
    id: app
    property var currentServiceUuid: ""
    width: 360
    height: 600
    visible: true
    title: "BLE ToolBox"
    header: ToolBar {
        id: toolBarItem
        RowLayout {
            spacing: 20
            anchors.fill: parent
            ToolButton {
                enabled: stack.depth > 1
                text: MdiFont.Icon.arrowLeft
                font.family: "Material Design Icons"
                font.pointSize: 18
                font.bold: true
                onClicked: {
                    if (stack.depth > 1) {
                        stack.pop()
                        //                                    listView.currentIndex = -1
                    } else {
                        //                                    drawer.open()
                    }
                }
            }
            Label {
                id: titleLabel
                text: "BLE Devices"
                font.pixelSize: 18
                elide: Label.ElideRight
                horizontalAlignment: Qt.AlignHCenter
                verticalAlignment: Qt.AlignVCenter
                Layout.fillWidth: true
            }
            ToolButton {
                id:scanButton
                property bool scanning: true
                //                visible: !scanIndicator.running
                text: scanning ? MdiFont.Icon.stop : MdiFont.Icon.refresh
                font.family: "Material Design Icons"
                font.pointSize: 20
                font.bold: true
                onClicked: {
                    if(scanning) {
                        scanButton.scanning = false
                        device.stopDeviceDiscovery()
                    } else {
                        scanButton.scanning = true
                        device.startDeviceDiscovery()
                    }
                }
                Component.onCompleted: {
                    device.startDeviceDiscovery()
                }
            }

            //            BusyIndicator {
            //                id: scanIndicator
            //                implicitHeight: refreshButton.width
            //                implicitWidth:  refreshButton.height
            //                visible: scanIndicator.running
            //                Component.onCompleted: {
            //                    device.startDeviceDiscovery()
            //                }
            //            }

        }
    }
    footer: Pane {
        id: footerItem
        visible: scanButton.scanning
        Material.elevation: 8
        ColumnLayout {
            anchors.fill: parent
            ProgressBar {
                Layout.fillWidth: true
                indeterminate: scanButton.scanning
            }
            Label {
                Layout.fillWidth: true
                Layout.preferredHeight: 20
                text: device.update
                font.pointSize: 9
            }
        }
    }
    Connections {
        target: device
        onStateChanged: {
            scanButton.scanning = device.state
        }
    }

    StackView {
        id: stack
        anchors.fill: parent
        initialItem: Page {
            Item {
                anchors.fill: parent
                ListView {
                    id: theListView
                    clip: true
                    enabled: !scanButton.scanning
                    anchors.fill: parent
                    model: device.devicesList
                    delegate: ItemDelegate {
                        width: parent.width
                        height: 60
                        onClicked: {
                            scanButton.scanning = true
                            device.scanServices(modelData.deviceAddress);
                            stack.push("qrc:/assets/Services.qml")
                        }
                        ColumnLayout {
                            anchors.fill: parent
                            ColumnLayout {
                                Layout.fillHeight: true
                                Layout.fillWidth: true
                                Layout.margins: 5
                                Label {
                                    id: deviceName
                                    text: modelData.deviceName
                                    font.pointSize: 13
                                    opacity: 0.87
                                }
                                Label {
                                    id: deviceAddress
                                    text: modelData.deviceAddress
                                    font.pointSize: deviceName.font.pointSize*0.7
                                    opacity: 0.56
                                }
                            }
                            Rectangle {
                                Layout.fillWidth: true
                                Layout.preferredHeight: 1
                                opacity: 0.1
                                color: Material.foreground
                            }
                        }
                    }
                }
            }
        }
    }
}
