/***************************************************************************
**
** Copyright (C) 2013 BlackBerry Limited. All rights reserved.
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the QtBluetooth module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.12
import Qt.labs.settings 1.0

Page {
    Component.onCompleted: {
        titleLabel.text = "Services list"
    }
    Connections {
        target: device
        onServicesUpdated: {
            console.log("Service updated")
        }
        onDisconnected: {
            console.log("disconnected")
            //scanButton.scanning = false
        }
        onServiceScanStateChanged: {
            console.log("Service scan changed to" + device.serviceScanState)
            scanButton.scanning = device.serviceScanState
        }
    }
    ListView {
        id: theServiceListView
        clip: true
        anchors.fill: parent
        model: device.servicesList
        delegate: ItemDelegate {
            width: parent.width
            height: 80
            onClicked: {
                scanButton.scanning = true
                app.currentServiceUuid = modelData.serviceUuid
                console.log(modelData.serviceUuid)
                device.connectToService(modelData.serviceUuid);
                stack.push("qrc:/assets/DeviceInfo.qml")
            }
            ColumnLayout {
                anchors.fill: parent
                ColumnLayout {
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Layout.margins: 5
                    Label {
                        id: serviceName
                        text: modelData.serviceName
                        font.pointSize: 14
                        opacity: 0.87
                    }
                    RowLayout {
                        Label {
                            text: modelData.serviceType
                            font.pointSize: serviceName.font.pointSize*0.7
                            opacity: 0.54
                        }
                        Label {
                            id: serviceUuid
                            text: modelData.serviceUuid
                            font.pointSize: serviceName.font.pointSize*0.7
                            opacity: 0.54
                        }
                    }
                }
                Rectangle {
                    Layout.fillWidth: true
                    Layout.preferredHeight: 1
                    opacity: 0.1
                    color: Material.foreground
                }
            }
        }
    }
}
